$(document).ready(function(){
	$('.js-top').click(function(e){
		e.preventDefault();
		$('html,body').animate({'scrollTop':'0'},300);
	});

	$('.visual__list').cycle({
		slides:'.visual__item',
		fx:'scrollHorz',
		loop:false,
		timeout:4000,
		swipe:true,
		pager:'.visual__pager',
		pagerTemplate:'<span class="visual__bull">&bull;</span>',
		pagerActiveClass:'is-active'
	});

	//코스메틱
	$('.js-slide').slick({
		autoplay: true,
		infinite: true,
		dots:true,
		slidesToShow:1,
		slidesToScroll: 1,
		dotsClass:'slide-dot',
		arrows:false
    });

	$("#sns_7").on('click',function(){
        var chk = $('input:checkbox[id="sns_7"]').is(":checked");
        if(chk==true){
            $(".form-box__etc").addClass("is-active");
			$(".input-etc").focus();
        }else{
			$(".form-box__etc").removeClass("is-active");
        }
    });

});
