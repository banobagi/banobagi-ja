$('.slide-type1__list').cycle({
	fx:'scrollHorz',
	swipe:true,
    slides:'.slide-type1__item',
	prev:'.slide-type1__btn-prev',
	next:'.slide-type1__btn-next',
	pager:'.slide-type1__pager',
	pagerTemplate:'<span class="slide-type1__pager-item">&bull;</span>',
	pagerActiveClass:'is-active',
	timeout:0,
	maxZ:10
});
