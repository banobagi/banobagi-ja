$(document).ready(function(){
	$(window).on("load resize", function () {
		headerH = $('.header').height();
		$('.js-quick-link').on('click',function(e){
			e.preventDefault();
			var dataName = $(this).attr('data-tab');
			var dataSet = $('.'+dataName).offset().top - headerH - 40;
			$("html, body").animate({
				scrollTop:dataSet
			},500);
		});
		//bfaf
		if ($('body').attr('only-mobile') == 'false' && $('body').attr('data-mobile') == 'true' && $('.js-bfaf-open').hasClass('is-active')){
			setTimeout(function() {
				$('.js-bfaf-open, .bfaf').removeClass('is-active');
				notiHide();
			},2000);
		}

		var thisH = $('.js-height').parent().innerHeight()-4;
		$('.js-height').height(thisH);
	});
	//bfaf
	function notiHide(){
		$('.side-noti').addClass('is-active');
		setTimeout(function() {
			$('.side-noti').removeClass('is-active');
		},2000);
	}
	$('.js-bfaf-open').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('is-active');
		$('.bfaf').toggleClass('is-active');
		if($(this).hasClass('is-active')){

		}else{
			notiHide();
		}
	});

	// new-visual effect
	$(".new-visual").addClass("is-active");

	$('.tab-v1__link').on('click',function(e){
		e.preventDefault();
		var tabClass = $(this).attr('href');
		$('.tab-v1__item, .tab-content').removeClass('is-active');
		$(this).parents('.tab-v1__item').addClass('is-active');
		$("."+tabClass).addClass('is-active');
	});

	//new-tab
	$('.new-tab__link').on('click',function(e){
		e.preventDefault();
		var tabClass = $(this).attr('href');
		$('.new-tab__item, .new-tab-content').removeClass('is-active');
		$(this).parents('.new-tab__item').addClass('is-active');
		$("."+tabClass).addClass('is-active');

		// tab 변경 시 matchHeight update
		$.fn.matchHeight._update();
	});

	//컨텐츠 바로가기
	$('.js-tab-go').on('click',function(e){
		e.preventDefault();
		var tabClass = $(this).attr('href');
		var tabTop = $('.new-tab').offset().top;
		var headerH = $('.header').height();
		var tapTop2 = tabTop - headerH;
		$('.new-tab__item, .new-tab-content').removeClass('is-active');
		$("."+tabClass).addClass('is-active');
		$('html, body').stop().animate({
			scrollTop: tapTop2
		}, 500);
	});

	//바디 인포
	$('.js-info').on('mouseenter',function(){
	    if ($('body').attr('data-mobile') == 'false'){
	        $(this).closest('.pop-info').find('.info-desc').stop().fadeIn(300);
	        $(this).addClass('is-active');
	    }
	}).on('mouseleave',function(){
	    if ($('body').attr('data-mobile') == 'false'){
	        $(this).closest('.pop-info').find('.info-desc').stop().fadeOut(300);
	        $(this).removeClass('is-active');
	    }
	}).on('click',function(e){
	    e.preventDefault();
	    if ($('body').attr('data-mobile') == 'true'){
	        if($(this).hasClass('is-active')){
	        }else{
	            $(this).addClass('is-active').find('.info-desc').fadeIn(300);
	            scrollStop();
	        }
	    }
	});

	$('.js-close').on('click',function(e){
		e.preventDefault();
		e.stopPropagation();
	    $(this).closest('.pop-info').find('.js-info').removeClass('is-active');
	    $(this).closest('.info-desc').fadeOut(300);
	    scrollStart();
	});

});

//박스 높이값 맞추기
$('.js-height, .js-op-height').matchHeight({
	byRow: true,
	target: null
});


//탭 모션
(function(){
	var tab = "false";
	var noti = "false";
	var notiOne = "false";
	var lineElem = document.querySelector('.new-tab__line');
	var lineElemJ = $('.new-tab__line');
	var tabElem = $('.new-tab');
	var contentElem = $('.new-tab-content');
	var listElem = $('.new-tab__list');
	var titElem = $('.new-tab__btn');
	var notiElem = $('.tab-noti');
	var itemElem = $('.new-tab__item');
	var itemH;
	var listH;
	$(window).on("load resize", function () {
		itemH = $('.new-tab__item').outerHeight();
		listH = itemH * itemElem.length;
	});
	function notiActive(){
		if(notiOne == "true") return;
		notiElem.fadeIn(300);
		function removeNoti(){notiElem.fadeOut(300);}
		setTimeout(removeNoti,1500);
		notiOne = "true";
	}
	titElem.on('click',function(e){
		e.preventDefault();
		if($(this).parents('.new-tab__tit').hasClass('is-active')){
			$(this).parents('.new-tab__tit').removeClass('is-active');
			listElem.css('top',-listH);

			tab = "false";
		}else{
			$(this).parents('.new-tab__tit').addClass('is-active');
			listElem.css('top',itemH-1);

			tab = "true";
		}
	});
	itemElem.on('click',function(e){
		var isState = $(this).parents('.new-tab').hasClass('is-fixed');
		if(isState == false) return;
		var currentText = $(this).find('.new-tab__link');
		titElem.text(currentText.text()).trigger('click');
		var lineTop = lineElem.offsetTop;
		$('html, body').stop().animate({
			scrollTop: lineTop+1
		}, 500);
	});
	function removeTab(){
		listElem.css('top',-listH);
		titElem.addClass('is-active');
	}
	function showValue(){

		var posY = lineElem.getBoundingClientRect().top;
		if(posY < 0){
			tabElem.addClass('is-fixed');
			contentElem.addClass('is-fixed');
			if(tab == "true") return;
			setTimeout(removeTab,100);
			if(noti == "true") return;
			if ($('body').attr('data-mobile') == 'true'){
				setTimeout(notiActive,200);
				noti = "true";
			}
		}else{
			noti = "false";
			tabElem.removeClass('is-fixed');
			contentElem.removeClass('is-fixed');
			listElem.css('top',itemH-1);
			titElem.removeClass('is-active');
		}
	}
	$(window).on('load',function(){
		var firstText = itemElem.eq(0).find('.new-tab__link');
		titElem.text(firstText.text());
	});
	$(window).on('scroll resize load',function(){
		try{showValue();}catch(e){}
	});
})();
