$(document).ready(function(){
	$('.js-slide').slick({
		autoplay: true,
		infinite: true,
		dots:true,
		slidesToShow:1,
		slidesToScroll: 1,
		dotsClass:'slide-dot',
		arrows:false
    });

	$("#sns_8").on('click',function(){
        var chk = $('input:checkbox[id="sns_8"]').is(":checked");
        if(chk==true){
            $(".form-box__etc").addClass("is-active");
			$(".input-etc").focus();
        }else{
			$(".form-box__etc").removeClass("is-active");
        }
    });
});
