$(document).ready(function(){

	//시계
	function digital(){
		var d = new Date(); /* d = 현재 시간 */
		var h = d.getHours(); /* h = 시간 */
		var m = d.getMinutes(); /* m = 분 */
		var s = d.getSeconds(); /* s = 초 */

		/* 시간을 12시간 단위로 하기 위해 12보다 큰 경우 12를 빼줌 */
		if( h > 12 ) h = h - 12;

		var dh = ( h * 30 ) + ( m / 2 ); /* dh = 시침의 각도 */
		dh = parseInt( dh ); /* 시침의 각도를 정수형으로 변환 */
		var dm = m * 6; /* 분침의 각도 */
		var ds = s * 6; /* 초침의 각도 */
		/* 시침, 분침, 초침의 각도를 계산한 값으로 변경 */
		$('.search-info__hour').css('transform','rotate('+ dh +'deg)');
		$('.search-info__minute').css('transform','rotate('+ dm +'deg)');
		$('.search-info__second').css('transform','rotate('+ ds +'deg)');
	}

	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);

		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);

		//전담팀 관련 js
		if ($('body').attr('data-mobile') == 'false'){
			$('.js-doctor-hover').on('mouseenter',function(){
				$(this).addClass('is-active');
			}).on('mouseleave',function(){
				$(this).removeClass('is-active');
			});
		}else{
			$('.js-doctor-hover').off('mouseenter mouseleave');
		}

		if ($('body').attr('only-mobile') == 'false'){
			var lnbW = $('.lnb__item.type-active').width();
			var lnbW2 = $('.lnb__2depth-item').width();
			if(lnbW > lnbW2 ){
				$('.lnb__2depth-item').width(lnbW+1);
			}else{
				$('.lnb__item.type-active').width(lnbW2+1);
			}
			$('.dim.type-lnb').removeClass('is-active');
			//검색결과 말줄임
			cellNoWrap2($('.js-news-ellipsis1'));
			cellNoWrap3($('.js-news-ellipsis2'));
			//모바일에서 스크롤멈춘상태에서 pc로 전환시 다시 스크롤시작
			if($('body').attr('class') == 'no-scroll'){
				scrollStart();
			}
		}else{
			$('.lnb__2depth-item').css('width','auto');
			//PC에서 로컬네비킨상태로 모바일 같을떄 딤처리
			var lnbClass = $('.lnb__2depth-list').attr('class');
			try {
				var lnbInfo = lnbClass.split(' ');
				if(lnbInfo[1] == 'is-active'){
					$('.dim.type-lnb').addClass('is-active');
				}
			} catch(error) {}
			//모바일에서 로케네비 온상태일때 다른곳클릭시 닫기
			$(document).off('click touchmove').on('click touchmove',function(e){
				try {
					var lnbClass = $('.lnb__2depth-list').attr('class');
					var lnbInfo = lnbClass.split(' ');
					if(lnbInfo[1] == 'is-active'){
						var lnbItem = $(".lnb");
						if( lnbItem.has(e.target).length === 0){
							$('.lnb__2depth-list, .dim.type-lnb').removeClass('is-active');
							scrollStart();
						}
					}
				} catch(error) {}

			});
			//검색결과 말줄임
			cellNormal($('.js-news-ellipsis1, .js-news-ellipsis2'));
		}
		var footerH = $('.footer').height();
		$('body').css('padding-bottom',footerH);
		if ($('body').attr('data-mobile') == 'false'){
			$('.header').removeClass('js-open-m'); //모바일 메뉴 오픈되어잇는 상태에서 PC로 돌아갈때 해제
			$('.m-search, .header__bg').removeClass('is-active');//모바일 메뉴 오픈되어잇는 상태에서 PC로 돌아갈때 해제
			$('.m-search__close').removeClass('animated is-active'); //모바일 메뉴 오픈되어잇는 상태에서 PC로 돌아갈때 해제
			//gnb 외 클릭시 닫기
			$(document).off('click').on('click',function(e){
				var container = $(".header, .gnb__depth2-wrap");
				if( container.has(e.target).length === 0){
					$('.gnb__all, .gnb__depth1-item').removeClass('js-open-d');
				}
			});

		}else{
			$('.header-search').removeClass('is-active');
		}

	});

	/*gnb*/
	var gnb = $('.gnb');
	$('.gnb__depth1-link').on("mouseenter",function(e) {
		var nowDepth = $(this).attr('class');
		var nowDepth2 = nowDepth.split(' ');
		if(nowDepth2[1] != 'type-1depth'){
			e.preventDefault();
			var gnbState = $(this).parents('.gnb__depth1-item');
			gnbState.addClass('js-open-d js-open-menu').siblings().removeClass('js-open-d js-open-menu');
			$('.gnb__depth1-item').addClass('js-open-all');
			// oneBulletRemove();
		}
	});
	//마우스아웃시 gnb 닫기
	$('.gnb, .gnb__depth2-wrap').on("mouseleave",function(e){
		$('.gnb__depth1-item').removeClass('js-open-all js-open-menu');
	});
	$('.gnb__depth2-item.type-3depth .gnb__depth2-link').on('click',function(e){
		e.preventDefault();
		var depth2Elam = $(this).closest('.gnb__depth2-item.type-3depth');
		if(depth2Elam.hasClass('is-open')){
			depth2Elam.removeClass('is-open is-fix-open');
			depth2Elam.find('.gnb__depth3-list').slideUp(300);
		}else{
			depth2Elam.addClass('is-open');
			depth2Elam.find('.gnb__depth3-list').slideDown(300);
		}
	});
	//바노바기 링크
	$('.js-bano-link').on("mouseenter",function(e){
		e.preventDefault();
		var gnbBano = $('.gnb__depth1-item.type-bano');
		gnbBano.addClass('js-open-d js-open-menu').siblings().removeClass('js-open-d js-open-menu');
		$('.gnb__depth1-item').addClass('js-open-all');
		$('.gnb__depth1-list').removeClass('is-hide');
		$('.search_inner').removeClass('open');
		// oneBulletRemove();
	});
	//리뷰 링크
	$('.js-review-link').on("mouseenter",function(e){
		e.preventDefault();
		var gnbBano = $('.gnb__depth1-item.type-review');
		gnbBano.addClass('js-open-d js-open-menu').siblings().removeClass('js-open-d js-open-menu');
		$('.gnb__depth1-item').addClass('js-open-all');

		$('.gnb__depth1-list').removeClass('is-hide');
		$('.search_inner').removeClass('open');
		// oneBulletRemove();
	});
	//글로벌,로그인
	$('.js-global, .js-login').on('click',function(e){
		e.preventDefault();
		$(this).parents('.snb__item').toggleClass('is-active');
	});
	//전체메뉴
	$('.js-all-menu').off('click').on('click',function(e){
		e.preventDefault();
		$(this).parents('.gnb__all').toggleClass('js-open-d');
		$('.gnb__depth1-item').removeClass('js-open-d js-open-all js-open-menu');
	});

	//전체메뉴 스크롤바
	//$('.total__depth-scroll').mCustomScrollbar({
	//	theme:'dark'
	//})

	//gnb 이미지 롤링
	$('.gnb__depth2-img').owlCarousel({
		loop:true,
		items:1,
		nav:false,
		dotsClass:'gnb-bullet',
		dotClass:'gnb-bullet__item',
		autoplay:true,
		autoplayTimeout:3000,
		autoplaySpeed:800
	});
	//하나인 경우 삭제
	function oneBulletRemove(){
		var gnbNum = $('.gnb__depth1-item.js-open-d .gnb__full-img .gnb-bullet__item').length;
		if(gnbNum == 1){
			$('.gnb-bullet').hide();
		}else{
			$('.gnb-bullet').show();
		}
	}


	/*검색*/
	var hederMenu = $('.header__wrap');
	$('.js-search').on('click',function(e){
		e.preventDefault();
		if ($('body').attr('data-mobile') == 'false'){
			$('.header-search').toggleClass('is-active');
			if ($('.header-search').hasClass('is-active')) {
				$('.header-search__group').addClass('animated fadeInRight');
				$('#keyword').focus();
				hederMenu.addClass('is-hide');
			} else {
				hederMenu.removeClass('is-hide');
			}
		}else{
			$('.m-search, .m-search__close').addClass('animated is-active');
			$('.header__bg').addClass('is-active');
		}
	});
	$('.js-search-close').on('click',function(e){
		e.preventDefault();
		$('.header-search').removeClass('is-active');
		hederMenu.removeClass('is-hide');
	});

	$('.mobile-menu').on('click',function(e){
		e.preventDefault();
		$('.header').toggleClass('js-open-m');
		if($('.header').hasClass('js-open-m')){
			$('#progress-indicator').addClass('is-hide');
			scrollStop();
			if ($('body').attr('data-mobile') == 'true' && $('body').attr('only-mobile') == 'false'){
				timer = setInterval( function(){ digital(); }, 1000);
			}
			//모바일메뉴 클릭시 해당 세션으로 포커스
			var nowDepth = $('.m-1depth__item.is-active').position().top;
			$('.m-1depth').stop().animate({
				scrollTop: nowDepth
			}, 0);
		}else{
			$('.m-search, .header__bg').removeClass('is-active');
			$('.m-search__close').removeClass('animated is-active');
			$('#progress-indicator').removeClass('is-hide');
			scrollStart();
			if ($('body').attr('data-mobile') == 'true' && $('body').attr('only-mobile') == 'false'){
				clearInterval(timer);
			}
		}
	});

	$('.m-1depth__link').on('click',function(e){
		e.preventDefault();
		var tabClass = $(this).attr('data-tab');
		$('.m-1depth__item, .m-2depth__list').removeClass('is-active');
		$(this).parents('.m-1depth__item').addClass('is-active');
		$("."+tabClass).addClass('is-active');
	});

	$('.type-3depth .m-2depth__link').on('click',function(e){
		e.preventDefault();
		$(this).parent().toggleClass('is-active');
	});

	$('.js-keyord-link').on('click',function(e){
		e.preventDefault();
		var tabClass = $(this).attr('data-tab');
		$('.keyword-box__list, .keyword-box2__list, .keyword-tab__item, .keyword-tab2__item').removeClass('is-active');
		$(this).addClass('is-active');
		$("."+tabClass).addClass('is-active');
	});

	$('.js-search-close').on('click',function(e){
		e.preventDefault();
		$('.m-search, .header__bg').removeClass('is-active');
		$('.m-search__close').removeClass('animated is-active');
	});

	$('.js-lnb').on('click',function(e){
		e.preventDefault();
		if($('.lnb__2depth-list, .dim.type-lnb').hasClass('is-active')){
			$('.lnb__2depth-list').removeClass('is-active');
			if ($('body').attr('only-mobile') == 'true'){
				$('.dim.type-lnb').removeClass('is-active');
				scrollStart();
			}
		}else{
			$('.lnb__2depth-list').addClass('is-active');
			if ($('body').attr('only-mobile') == 'true'){
				$('.dim.type-lnb').addClass('is-active');
				scrollStop();
			}
		}
	});

	//로그인 바노바기
	$('.js-bano-close').on('click',function(e){
		e.preventDefault();
		$(this).parents('.p-login__layer').removeClass('is-active');
	});
	$('.js-open-layer').on('click',function(e){
		e.preventDefault();
		$('.p-login__layer').addClass('is-active');
	});
	//로그인 open/close
	$('.js-login-close').on('click',function(e){
		e.preventDefault();
		$('.p-login, .p-login-dim').fadeOut(500);
		scrollStart();
	});
	$('.js-login-open').on('click',function(e){
		e.preventDefault();
		$('.p-login, .p-login-dim').fadeIn(500);
		scrollStop();
	});

	$('.js-bfaf-link').on('click',function(e){
		e.preventDefault();
		$(this).parent().find('.pics-link__list').toggleClass('is-active');
	});

	// 전체메뉴 스크롤
	$(window).on("load",function(){
	    $('.total__depth-scroll').mCustomScrollbar({
	        theme:"minimal-dark"
	    });
	});

	scroll_button();
	function scroll_button(){
	   var $button = $('.scroll-top');

	   if(!$button.length) { return; }
	   var scrollBtnW = $button.width();
	   // circle progress scroll
	   $.circleProgress.defaults.animation = false;
	   $.circleProgress.defaults.value = 0;
	   $.circleProgress.defaults.size = scrollBtnW;
	   $.circleProgress.defaults.startAngle = -Math.PI / 4 * 2;
	   $.circleProgress.defaults.thickness = '4';
	   $.circleProgress.defaults.emptyFill = 'rgba(221, 221, 221, 0.9)';
	   $.circleProgress.defaults.fill = { color: '#b916a4' };


	   $('.scroll-top__progress').circleProgress();

	   $(window).on('load resize',function(){
		   scroll_button_resize();
	   });
	   $button.on('click', function(e) {
		   e.preventDefault();
		   $('html, body').stop().animate({
			   scrollTop: 0
		   }, 500);
	   });
	}

	function scroll_button_resize(){
	   $('.scroll-top__progress').circleProgress('redraw');
	}

	//progressBar
	$.progressIndicator({
	   direction : 'top',
	   barColor: 'rgb(255, 132, 0)',
	   percentageEnabled : false,
	   percentageColor: '#222',
	   easingSpeed : 0.5,
	   height: 5,
	   target : 'body', // selector
	   onStart : function(){
	   },
	   onEnd : function(){
	   },
	   onProgress : function(perecent){
	   }
	});

});
//리뷰 슬라이드
$('.gnb-slide2').owlCarousel({
    loop:true,
    items:1,
    nav:true,
    dotsClass:'gnb-bullet',
    dotClass:'gnb-bullet__item',
});

// 빠른 상담
var bodyY;
var isPage;
var isData;
function scrollStop(){
	bodyY = $(window).scrollTop();
	isPage = $('body').attr('page-type');
	isData = $('body').attr('data-mobile');
	if(isPage == 'detail' && isData == 'false'){
		$.scrollify.disable();
		return;
	}
	$('html, body').addClass("no-scroll");
	$('.common').css("top",-bodyY);
}
function scrollStart(){
	if(isPage == 'detail' && isData == 'false'){
		$.scrollify.enable();
		return;
	}
	$('html, body').removeClass("no-scroll");
	$('.common').css('top','auto');
	bodyY = $('html,body').scrollTop(bodyY);

}

//원페이지 팝업
var posY;
$('.js-popup-open').on('click',function(e){
	e.preventDefault();
	if ($('body').attr('data-mobile') == 'false' || $(this).attr('data-popup-type') == 'section' ){
		$(this).parents('.fs_section').find('.detail__popup').fadeIn();
		$(this).parents('.fs_section').find('.detail__popup-m, .detail__dim').fadeToggle();
		if($(this).attr('data-popup-type') == 'section'){$(this).toggleClass('is-active');}//큰팝업 엑스회전 막기
	}else{
		$(this).parents('.fs_section').find('.detail__popup').fadeIn();
		$(this).parents('.fs_section').find('.detail__popup-m, .detail__dim').fadeToggle();
		scrollStop();
		$('.footer').addClass('is-active');
		if($(this).attr('data-popup-type') == 'footer'){
			$('.footer').removeClass('is-active');
		}
	}
});
$('.js-popup-close').on('click',function(e){
	e.preventDefault();
	if ($('body').attr('data-mobile') == 'false' || $(this).attr('data-popup-type') == 'section' ){
		$('.detail__popup').fadeOut();
		$('.js-popup-open').removeClass('is-active');
		$(this).parents('.fs_section').find('.detail__dim').fadeToggle();
	}else{
		$('.detail__popup').fadeOut();
		scrollStart();
		$('.footer').removeClass('is-active');
		$(this).parents('.fs_section').find('.detail__dim').fadeToggle();
	}
});



//상담신청
$('.js-c-open').on('click',function(e){
	e.preventDefault();
	$('.popup-c').fadeIn(500);
});
$('.js-c-close').on('click',function(e){
	e.preventDefault();
	$('.popup-c').fadeOut(500);
});
$('.popup-c__link').on('click',function(e){
	e.preventDefault();
	var tab_id = $(this).attr('data-tab');
	$('.popup-c__item').removeClass('is-active');
	$('.popup-c__content').removeClass('is-active');
	$(this).parent().addClass('is-active');
	$(".popup-c__content"+"."+tab_id).addClass('is-active');
});

$('.js-consultation').click(function(e) {
	e.preventDefault();
	$('.header__right').addClass('remove-phone');
	$('.popup-c.type-common').fadeIn(300);
	scrollStop();
});
$('.js-consultation-event').click(function(e) {
	e.preventDefault();
	$('.popup-c.type-event').fadeIn(300);
	scrollStop();
});
$('.popup-c__close').on('click',function(e){
	e.preventDefault();
	$('.input1__clear').hide();
	$('.popup-c').fadeOut(300);
	scrollStart();
});


//상담신청
$('.js-c-open').on('click',function(e){
	e.preventDefault();
	$('.popup-c').fadeIn(500);
});
$('.js-c-close').on('click',function(e){
	e.preventDefault();
	$('.popup-c').fadeOut(500);
});
$('.popup-tab__link').on('click',function(e){
	e.preventDefault();
	var tab_id = $(this).attr('data-tab');
	$('.popup-tab__item').removeClass('is-active');
	$('.popup-content').removeClass('is-active');
	$(this).parent().addClass('is-active');
	$(".popup-content"+"."+tab_id).addClass('is-active');
	$('.input1__clear').hide();
});

//input 닫기
var $ipt = $('.popup-input1'),
    $clearIpt = $('.input1__clear');

$ipt.keyup(function(){
	$(this).next().toggle(Boolean($(this).val()));
});

//select 박스
$clearIpt.toggle(Boolean($ipt.val()));
$clearIpt.click(function(e){
	e.preventDefault();
	$(this).prev().val('').focus();
	$(this).hide();
});

$('.popup-select__item').on('mouseenter',function(){
	$(this).addClass('is-hover');
}).on('mouseleave',function(){
	$(this).removeClass('is-hover');
});
function innerScrollStop(){
	$('.popup-c__box').addClass('no-scroll');
}
function innerScrollStart(){
	$('.popup-c__box').removeClass('no-scroll');
}
$(".popup-select__tit").click(function() {
	$('.popup-select, .popup-select2__subject, .popup-select__list').removeClass('is-active');
	$(this).parent().addClass('is-active');
	$(this).parent().find('.popup-select2__subject, .popup-select__list').addClass('is-active');
	innerScrollStop();
    return false;
});

$(".popup-select2__subject").click(function() {
	$(this).parent().removeClass('is-active');
	$(this).parent().find('.popup-select2__subject, .popup-select__list').removeClass('is-active');
	innerScrollStart();
    return false;
});

//기본 탭 링크
$('.js-tab-link').on('click',function(e){
	e.preventDefault();
	var tab_id = $(this).parent().attr('data-tab');
	$(this).parent().addClass('is-active').siblings().removeClass('is-active');
	$("."+tab_id).addClass('is-active').siblings().removeClass('is-active');
	if ($('body').attr('only-mobile') == 'false'){
		cellNoWrap2($('.js-news-ellipsis1'));
		cellNoWrap3($('.js-news-ellipsis2'));
	}else{
		cellNormal($('.js-news-ellipsis1, .js-news-ellipsis2'));
	}
});
//의료진소개 쪽 탭 링크
$('.js-tab-link2').on('click',function(e){
	e.preventDefault();
	var tab_id = $(this).parent().attr('data-tab');
	$(this).parent().addClass('is-active').siblings().removeClass('is-active');
	$(this).closest('.medical-view__item').find("."+tab_id).addClass('is-active').addClass('is-active').siblings().removeClass('is-active');
});

//팝업 스크롤
// $('.popup-select__list, .terms-c__area').mCustomScrollbar({
// 	theme:'minimal'
// });

//카톡상담
$('.js-katalk').on('click',function(e){
    e.preventDefault();
    $('.js-consultation').trigger('click');
    $('.popup-tab__item').eq(1).find('.popup-tab__link').trigger('click');
});
//비용상담
$('.js-cost').on('click',function(e){
    e.preventDefault();
    $('.js-consultation').trigger('click');
    $('.popup-tab__item').eq(2).find('.popup-tab__link').trigger('click');
});



//온라인상담의 상담항목 선택
$("#onlineForm .popup-select__item").click(function() {
	$(this).parents('.popup-select__list').removeClass('is-active').parents(".popup-select").removeClass('is-active').find(".popup-select__tit").text($(this).text());
	$(this).parents('.popup-select').find('.popup-select2__subject').removeClass('is-active');
	$("#onlineForm .popup-select__item").removeClass('select');
	$(this).addClass('select');
	var idx = $(this).attr('id').split('_')[1];
	$('#onlinePartItem').val(idx).keyup();
});

//온라인상담의 상담항목 선택
$("#kakaoForm .popup-select__item").click(function() {
	$(this).parents('.popup-select__list').removeClass('is-active').parents(".popup-select").removeClass('is-active').find(".popup-select__tit").text($(this).text());
	$(this).parents('.popup-select').find('.popup-select2__subject').removeClass('is-active');
	$("#onlineForm .popup-select__item").removeClass('select');
	$(this).addClass('select');
	var idx = $(this).attr('id').split('_')[1];
	$('#kakaoPartItem').val(idx).keyup();
});
//이벤트
$("#onlineForm2 .popup-select__item").click(function() {
	$(this).parents('.popup-select__list').removeClass('is-active').parents(".popup-select").removeClass('is-active').find(".popup-select__tit").text($(this).text());
	$(this).parents('.popup-select').find('.popup-select2__subject').removeClass('is-active');
	$("#onlineForm .popup-select__item").removeClass('select');
	$(this).addClass('select');
	var idx = $(this).attr('id').split('_')[1];
	$('#onlinePartItem').val(idx).keyup();
	innerScrollStart();
});

$('.js-terms-open').on('click',function(e){
	e.preventDefault();
	$(this).closest('.popup-c').find('.popup-c__terms').addClass('is-active');
});
$('.js-terms-close').on('click',function(e){
	e.preventDefault();
	$('.popup-c__terms').removeClass('is-active');
});

//푸터 블로그 오버효과
$('.sns__link.type-blog').mouseenter(function() {
	$(this).find('img').attr('src', $(this).find('img').attr('src').replace('.png', '_on.png'));
}).mouseleave(function() {
	$(this).find('img').attr('src', $(this).find('img').attr('src').replace('_on.png', '.png'));
});



//테이블 말줄임
function cellNoWrap(target) {
  target.css({'max-width':target.parent().width(),'white-space':'nowrap'},function(){});
}
function cellNoWrap2(target) {
  target.css({'max-width':target.parent().width(),'white-space':'nowrap'},function(){});
}
function cellNoWrap3(target) {
  target.css({'max-width':target.parent().width(),'white-space':'nowrap'},function(){});
}

function cellNormal(target) {
  target.css({'max-width':'auto','white-space':'normal'});
}

$(document).ready(function(){
	cellNoWrap($('.js-board-link, .js-menu-link, .js-epilogue-tit'));
});

$(window).on('resize', function(e) {
	cellNormal($('.js-board-link, .js-menu-link, .js-epilogue-tit'));
	cellNoWrap($('.js-board-link, .js-menu-link, .js-epilogue-tit'));
}).resize();
//이벤트 팝업 상세입력 펼치기 버튼

$('.js-detail-open').on('click',function(e){
	var detailState = $('.popup-content__hide');
	e.preventDefault();
	if (detailState.hasClass('is-active')) {
		detailState.removeClass('is-active').slideUp(300);
		$('.popup-content__arrow').removeClass('is-active');

	}else{
		detailState.addClass('is-active').slideDown(300);
		$('.popup-content__arrow').addClass('is-active');
	}
});





//페이지 이동 함수
$.fnGoPage = function(page){
	$('#page').val(page);
	$('#frmBoard').attr({action:$('#goUrl').val(), method:'post'}).submit();
};



// 온라인 상담
$("#onlineForm").validate({
  errorPlacement: function(error, element) {
  	if (element.attr("name") == "chkAgree1") {
      error.insertAfter("#onlineForm .popup-content__btn2");
    }else if (element.attr("name") == "onlinePartItem") {
	  error.insertAfter("#onlinePartItem");
    } else {
      error.insertAfter(element);
    }
  },
   rules: {
      onlinePartItem: {required: true}
  },
  messages: {
    onlineName: "이름을 입력해주세요.",
    onlinePhone: "연락처를 입력해주세요.",
    onlinePartItem: "관심부위를 선택해주세요.",
    onlineContent: "상담내용을 입력해주세요.",
    chkAgree1: "개인정보취급방침에 동의해주세요."
  },
  submitHandler: function() {
	// form.submit();
	//gtag('event', '카톡상담', {'event_category': '상담신청','event_label':'상담신청'});
	var formdata = $('#onlineForm').serialize();

    $.post('/inc/incCounselProc.php', formdata, function(data){
		if(data == 'errorCase1' )
		{
		  alert('필수입력 항목이 누락되었습니다!!');
		}else if(data == 'errorCase2'){
		  alert('상담신청에 실패하였습니다!!');
		}else{
		 // alert(data);
		  $('#onlineForm')[0].reset();
		  $('#onlineForm .popup-select__tit').text('관심부위를 선택해주세요');
		  $('#onlineForm .popup-select__item').removeClass('select');
		  $('#onlinePartItem').val('');
		  alert('상담신청이 완료되었습니다!!');
		}
    });
  }
});

// 카톡 상담
$("#kakaoForm").validate({
  errorPlacement: function(error, element) {
  	if (element.attr("name") == "chkAgree2") {
		error.insertAfter("#kakaoForm .popup-content__btn2");
      }else if (element.attr("name") == "onlinePartItem") {
  	  error.insertAfter("#kakaoPartItem");
    } else {
      error.insertAfter(element);
    }
  },
   rules: {
      onlinePartItem: {required: true}
  },
  messages: {
    kakaoName: "이름을 입력해주세요.",
    kakaoPhone: "연락처를 입력해주세요.",
    kakaoPartItem: "관심부위를 선택해주세요.",
	kakaoContent: "상담내용을 입력해주세요.",
    chkAgree2: "개인정보취급방침에 동의해주세요."
  },
  submitHandler: function() {
	// form.submit();
	//gtag('event', '카톡상담', {'event_category': '상담신청','event_label':'상담신청'});
	var formdata = $('#kakaoForm').serialize();

    $.post('/inc/incCounselProc.php', formdata, function(data){
		if(data == 'errorCase1' )
		{
		  alert('필수입력 항목이 누락되었습니다!!');
		}else if(data == 'errorCase2'){
		  alert('상담신청에 실패하였습니다!!');
		}else{
		  $('#kakaoForm')[0].reset();
		  $('#kakaoForm .popup-select__tit').text('관심부위를 선택해주세요');
		  $('#kakaoForm .popup-select__item').removeClass('select');
		  $('#kakaoPartItem').val('');
		  alert('상담신청이 완료되었습니다!!');
		}
    });
  }
});

//탭바
$(document).ready(function(){
    $('.content-tab__link').on('click',function(e){
        e.preventDefault();
        var tabClass = $(this).attr('data-tab');
        $('.content-tab__item, .content-tab__page').removeClass('is-active');
        $(this).parents('.content-tab__item').addClass('is-active');
        $("."+tabClass).addClass('is-active');
    });
});
