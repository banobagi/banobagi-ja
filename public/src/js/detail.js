$(document).ready(function(){
	$(window).on("load resize", function () {
        $('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);
		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);

        function disableScrollify(toggle){
            if(toggle){
        		$('body').css('overflow','auto');
        		$('.fs_section.first').addClass('js-mobile');
                $.scrollify.disable();
            } else {
        		$('.fs_section.first').removeClass('js-mobile');
        		$.scrollify.enable();
                $.scrollify(settings);
            }
        }
        $.scrollify(settings);
        var firstSection = $('.fs_section').eq(0).attr('data-section-name');
        var lastSection = $('.fs_section').last().attr('data-section-name');
        //상단으로 가기
        $('.scroll-top').on('click', function(e) {
            e.preventDefault();
            $.scrollify.move("#home");
            if(firstSection == 'contour-section99'){
				$.scrollify.move("#contour-section99");
				videoPlay();
			}
        });

        if ($('body').attr('only-mobile') == 'true'){
			if(firstSection == 'contour-section99' || lastSection == 'wrinkleSection06'){
				videoReset();
			}
		} else {
			disableScrollify(0);
            if(firstSection == 'contour-section99' || lastSection == 'wrinkleSection06'){
				playCheck();
			}
		}
		if ($('body').attr('data-mobile') == 'true'){
            if(firstSection == 'contour-section99' || lastSection == 'wrinkleSection06'){
				videoStop();
				// onepage_player.setLoop(true);
			}
		}else{
			if(firstSection == 'contour-section99'){
				// videoPlay();
				// onepage_player.setLoop(false);
			}
		}

        if($(window).width() < 681){
            disableScrollify(1);
        } else {
            disableScrollify(0);
        }

        $(window).resize(function() {
            if($(window).width() < 681){
                disableScrollify(1);
            }else{
                disableScrollify(0);
            }
        });

    });//load,resize
    //영상플레이
    var dataVideo = $('.detail-common').attr('data-video');
    var settings = {
        section:".fs_section",
        scrollbars:false,
        scrollSpeed: 700,
        // offset : 0,
        updateHash: true,
        interstitialSection:".footer",
        before:function(i,panels) { //https://player.vimeo.com/video/265675036?loop=1&amp;quality=1080p&amp

            var ref = panels[i].attr("data-section-name");
            //로딩전
            if(dataVideo =="true"){
                if (ref == 'wrinkleSection06' || ref == 'contour-section99'){
                    videoPlay();
                } else {
                    videoStop();
                }
            }
          $(".fs_nav .active").removeClass("active");
          $(".fs_nav").find("a[href=\"#" + ref + "\"]").addClass("active");
        },
        afterRender:function() {
          var pagination = "<ul class=\"fs_nav\">";
          var activeClass = "";
          $(".fs_section").each(function(i) {
            activeClass = "";
            if(i===0) {
              activeClass = "active";
          }
            pagination += "<li><a class=\"" + activeClass + "\" href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-name").charAt(0).toUpperCase() + $(this).attr("data-section-name").slice(1) + "</span></a></li>";
          });

          pagination += "</ul>";

          $(".fs_section.first, .fs_section.js-first").append(pagination);

          $(".fs_nav a").on("click",$.scrollify.move);
        },after:function(i,panels) {
            var ref = panels[i].attr("data-section-name");
            //로딩후
            if(dataVideo =="true"){
                if (ref == 'wrinkleSection06' || ref=="contour-section99"){
                    videoPlay();
                }
            }
        }
    };

    try {
        var onepage_iframe = $("#onepage-video__iframe");
        var onepage_player = new Vimeo.Player(onepage_iframe);

        function playCheck(){
            onepage_player.on("ended", function() {
                // console.log("ended!");
                // $.scrollify.move("#home");
            });
        }
        //영상플레이
        function videoPlay() {
            onepage_player.play();
        }
        //영상 일시정지
        function videoStop() {
             onepage_player.pause();
        }
        function videoReset(){
            onepage_player.setCurrentTime(0);
            onepage_player.pause();
        }
    }catch(error) {}

    //메뉴오픈시 디테일 스크롤 움직임 막기
    $('.mobile-menu').on('click',function(e){
        if ($('body').attr('data-mobile') == 'true' && $('body').attr('only-mobile') == 'false'){
            if($('.header').hasClass('js-open-m')){
                $.scrollify.enable();
            }else{
                $.scrollify.disable();
            }
        }
    });
});













$(window).on('resize', function() {
  if($(window).width()>992 && $('.fs_nav').hasClass('hide') && !$('.ad_menu').hasClass('open')) {
    $.scrollify.enable();
    $('.fs_nav').removeClass('hide');
  }
  // 섹션별 스크롤 막은것 해제
  $('html, #cbp-hrmenu > ul > li a').click(function(e) {
    if($(window).width()>992) {
      var container1 = $('header .sideMenu .ad_menu');

      if (!container1.is(e.target) && container1.has(e.target).length === 0) {
        $.scrollify.enable();
        $('.fs_nav').removeClass('hide');
      }
    }
  });

});

$(window).on('load',function(){
	$('body').attr('page-type','detail');
});



$(window).on("load resize", function () {
	$('body').attr('data-mobile',
		(function(){
			var r = ($(window).width() <= 668) ? true : false;
			return r;
		}())
	);
	var winH = $(window).height();
	$('.fs_section.js-first').height(winH);

});
